package org.green.kav.activemq.queue.consumer;

import org.green.kav.activemq.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    private Logger log = LoggerFactory.getLogger(Consumer.class);

    @JmsListener(destination = "messageBoard", containerFactory = "myFactory")
    public void receiveMessage(Message msg) {
        log.info("msg: {}", msg);
    }

    @JmsListener(destination = "International.Call", containerFactory = "myFactory")
    public void international(Object msg) {
        log.info("msg: {}", msg);
    }
}
