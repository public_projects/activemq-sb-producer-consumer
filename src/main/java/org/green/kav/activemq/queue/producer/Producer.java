package org.green.kav.activemq.queue.producer;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQQueue;
import org.green.kav.activemq.config.Config;
import org.green.kav.activemq.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import javax.jms.JMSException;
import java.util.Set;

@Component
public class Producer implements CommandLineRunner {
    private Logger log = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Config config;

    @Override
    public void run(String... args) throws Exception {
        // Send a message with a POJO - the template reuse the message converter
        log.info("Sending an message.");
        jmsTemplate.convertAndSend("messageBoard", new Message("info@example.com", "Hello"));

        // Get all the queue from activeMQ. NOTE: only works if run in debug!
        getAllOtherQueuesInActiveMq();
    }

    private void getAllOtherQueuesInActiveMq() {
        log.info("getting all queues");
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(config.getBrokerUrl());
            ActiveMQConnection connection = (ActiveMQConnection) connectionFactory.createConnection();

            connection.start();

            DestinationSource ds = connection.getDestinationSource();
            Set<ActiveMQQueue> queues = ds.getQueues();
            Thread.sleep(15000l); // this makes sure all the queue was collection!
            log.info("Queue count {}", queues != null ? queues.size() : 0);
            StringBuilder sb = new StringBuilder(System.lineSeparator());
            for (ActiveMQQueue activeMQQueue : queues) {
                try {
                    sb.append("from(\"" + activeMQQueue.getQueueName() + "\").process(new GeneralLoggingProcessor(\"" +
                              activeMQQueue.getQueueName() + "\"));").append(System.lineSeparator());
                    log.info("queue{}", activeMQQueue.getQueueName());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
            log.info("{}", sb);
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
